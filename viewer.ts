interface IResize
{
    resize(): void;
}

class Control implements IResize
{
    private _container: HTMLElement;
    private _filePicker: HTMLInputElement;
    private _canvas: Canvas;

    constructor(container: string, filePicker: string)
    {
        this._container = E(container);
        this._filePicker = E(filePicker);
        this._canvas = null;

        this._filePicker.addEventListener("change", () =>
        {
            var files = this._filePicker.files;
            if (validateFiles(files))
                this._canvas.file = files[0];
        });
    }

    set canvas(value: Canvas)
    {
        this._canvas = value;
    }

    get height(): number
    {
        return this._container.offsetHeight;
    }

    get width(): number
    {
        return this._container.offsetWidth;
    }

    public resize(): void
    {
        this._container.style.width = `${window.innerWidth}px`;
    }
}

class Canvas implements IResize
{
    private _canvas: HTMLCanvasElement;
    private _ctx: CanvasRenderingContext2D;
    private _controls: Control;

    private _file: File;
    private _img: HTMLImageElement;

    constructor(canvas: string)
    {
        this._canvas = E(canvas);
        this._ctx = this._canvas.getContext("2d");
        this._controls = null;
        this._img = null;

        this._canvas.addEventListener("dragover", (evt: DragEvent) =>
        {
            evt.stopPropagation();
            evt.preventDefault();
            evt.dataTransfer.dropEffect = "copy";
        });

        this._canvas.addEventListener("drop", (evt: DragEvent) =>
        {
            evt.stopPropagation();
            evt.preventDefault();

            var files = evt.dataTransfer.files;
            if (validateFiles(files))
                this.file = files[0];
        });
    }

    set controls(value: Control)
    {
        this._controls = value;
    }

    set file(value: File)
    {
        this._file = value;
        console.log(`Selected: ${this._file.name}`);

        var reader = new FileReader();
        reader.addEventListener("load", (evt: Event) =>
        {
            this._img = new Image();

            this._img.addEventListener("load", () =>
            {
                this.updateMargins();
                this.render();
            });

            this._img.src = reader.result as string;
        });

        reader.readAsDataURL(this._file);
    }

    public resize(): void
    {
        var newHeight = window.innerHeight - this._controls.height - 1;
        this._ctx.canvas.height = newHeight;
        this._ctx.canvas.width = this._controls.width;
    }

    private updateMargins(): void
    {
        var canvas = this._ctx.canvas;
        if (this._img.height > window.innerHeight)
        {
            canvas.style.marginBottom = `${this._controls.height}px`;
        }
        else
        {
            canvas.style.marginBottom = "0px";
        }
    }

    public render(): void
    {
        this._ctx.canvas.setAttribute("width", this._img.width.toString());
        this._ctx.canvas.setAttribute("height", this._img.height.toString());

        var width = this._ctx.canvas.width;
        var height = this._ctx.canvas.height;

        this._ctx.clearRect(0, 0, width, height);
        this._ctx.drawImage(this._img, 0, 0);
    }
}

function E(name)
{
    return document.querySelector(name);
}

function debounce(func, wait, immediate)
{
    var timeout;
    function inner()
    {
        const ctx = this;
        const args = arguments;
        const later = () =>
        {
            timeout = null;
            if (! immediate)
            {
                func.apply(ctx, args);
            }
        };

        const callNow = immediate && ! timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow)
        {
            func.apply(ctx, args);
        }
    }

    return inner;
}

function validateFiles(files): boolean
{
    return files && files[0];
}


window.addEventListener("load", () =>
{
    let controls = new Control("#mainControls", "#inFilePicker");
    let canvas = new Canvas("#canvas");

    controls.canvas = canvas;
    canvas.controls = controls;

    controls.resize();
    canvas.resize();

    window.addEventListener("resize", debounce(() =>
    {
        controls.resize();
        canvas.resize();
        canvas.render();
    }, 100, false), true);

    console.log("Loaded");
});